// Users Routes

const express = require('express');
const router = express.Router();

// Register
router.get('/register', (req, res, next) => {
  res.send('Register route: /users/register');
});

// Authenticate
router.post('/authenticate', (req, res, next) => {
  res.send('Authenticate route: /users/authenticate');
});

// Profile
router.get('/profile', (req, res, next) => {
  res.send('Profile route: /users/profile');
});

// Validate
router.get('/validate', (req, res, next) => {
  res.send('Validate route: /users/validate');
});


module.exports = router;
