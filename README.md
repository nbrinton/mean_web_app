# MEAN Authentication App
This project is a web app that demonstrates what basic authentication functionality created using
the MEAN web stack. The acronym MEAN stands for the first letter from each technology in the stack:
MongoDB - no-sql database, Express - back-end JavaScript web framework, Angular - front-end
JavaScript web framework, and Node - JavaScript runtime environment.

This project follows the [Traversy Media MEAN Auth App Tutorial](https://www.youtube.com/playlist?list=PLillGF-RfqbZMNtaOXJQiDebNXjVapWPZ) more or less.

## Dependencies
This project primarily requires Node and MongoDB to be installed on your system, with the rest of
the dependencies for this project being Node modules listed in a map called "dependencies" in the ```package.json file```. Below are links to the documentation for each module:

### Install Node
To install Node on your computer, follow these [instructions](http://blog.teamtreehouse.com/install-node-js-npm-windows) which are essentially these:

1) Go to the [Node Website](https://nodejs.org/en/) and click on the Download button for the latest
version for your computer's architecture.

2) Run the .msi installer file and fill out the appropriate values (most likely the defaults are
  fine)

3) Restart your computer to finish the installation

4) Test that Node and npm successfully installed by opening your terminal and running:
    ~~~
    node -v
    npm -v
    ~~~


### Install MongoDB
1) To install MongoDB on your computer, first go to the [MongoDB website](https://www.mongodb.com/).

2) Click on the button that says "Get MongoDB" and select the latest community release for your
  computer's architecture.

3) Run the installer and select the appropriate values (most likely the default values are fine).

### Node Modules

* [bcryptjs](https://www.npmjs.com/package/bcryptjs)
* [body-parser](https://www.npmjs.com/package/body-parser)
* [cors](https://www.npmjs.com/package/cors)
* [express](https://expressjs.com/)
* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
* [mongoose](https://mongoosejs.com/)
* [nodemon](https://github.com/remy/nodemon)
* [passport](http://www.passportjs.org/)
* [passport-jwt](https://www.npmjs.com/package/passport-jwt)

## Building and Running
You can run the project by entering:
~~~
npm start
~~~

However, if you choose to start this way then if you make any changes to the app you will need to
restart it for those changes to be reflected. This is why I installed the nodemon Node module which
monitors your project and whenever you make changes it automatically restarts the server so that
upon a web-browser refresh you can see your new changes without having to manually restart the
server. To run the project using Nodemon, enter:
~~~
nodemon
~~~
