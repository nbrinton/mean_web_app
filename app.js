// Import desired Node Modules ie "dependencies"
const express = require('express');
const path = require('path');//core module, so didn't need to install it in dependencies
const bodyParser = require('body-parser');// Parses incoming request bodies from forms etc. so you can grab data
const cors = require('cors');// Allows us to make requests to our API from a different domain name
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database')
// Note: nodemon is a module that monitors changes and keeps rerunning the server after each change

// Connect to database (MongoDB) that's stored in a config file
mongoose.connect(config.database);

// Check connection

// On successful connection, print a success message
mongoose.connection.on('connected', () => {
  console.log('Connected to database: ' + config.database)
});

// On an unsuccessful connction, print a failure message
mongoose.connection.on('error', (err) => {
  console.log('Error connecting to database: ' + err)
});


// Initilialize express
const app = express();

// Initialize routes folder
const users = require('./routes/users');

// Port number
const port = 3000;

// CORS middleware
app.use(cors());

// Set static folder (location for angular)
app.use(express.static(path.join(__dirname, 'public')));

// Body parser middleware
app.use(bodyParser.json());

// Routes

// Users routes
app.use('/users', users);

// Index route
app.get('/', (req, res) => {
  res.send('Index Route: /');
});

// Start server
app.listen(port, () => {
  console.log('Server started on port ' + port);
});

// both of these are equivalent callback functions, but the second (arrow function) is shorthand
// app.listen(port, function() {
//  console.log('Server started on port ' + port);
// })
